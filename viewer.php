<?php

# Variables
$useragent = $_SERVER['HTTP_USER_AGENT'];
$isDiscord = strpos($useragent, 'Discord') !== false;
$filename = end(explode("/", $_SERVER['REQUEST_URI']));
$size = filesize("i/" . $filename . ".png") / (1024 * 1024);

$tw_site = "@minecodes_";
$tw_creator = "@minecodes_";

# Check if file in the directory matches the string
if ($_SERVER['REQUEST_URI'] == "/") {
    readfile("index.html");
} else {
    if (file_exists("i/" . $filename . ".png")) {
        if ($isDiscord) {
            $title = $filename . ".png - " . $size . " MB";
            $oembed = "<link href=\"/oembed.json\" title=\"oEmbed\" rel=\"alternate\" type=\"application/json+oembed\" />";
            $twitter = "<meta property=\"twitter:site\" content=\"$tw_site\"><meta property=\"twitter:creator\" content=\"$tw_creator\">";
            $og_image = "<meta property=\"og:image\" content=\"/i" . $_SERVER['REQUEST_URI'] . ".png\" />$twitter";
            $og_title = "<meta property=\"og:title\" content=\"$title\" />";
            echo "<html><head>$og_image<title>$title</title>$og_title<meta name=\"twitter:card\" content=\"summary_large_image\">$oembed<meta content=\"#ff0040\" data-react-helmet=\"true\" name=\"theme-color\"></head></html>";
            exit();
        }
        header("Content-Type: image/png");
        readfile("i/" . $filename . ".png");
    } else {
        http_response_code(404);
        header("Content-Type: image/jpg");
        readfile("i/404.jpg");
    }
}

?>