#!/bin/bash
IMAGEPATH="$HOME/Pictures/" # Where to store screenshots before they're deleted
IMAGENAME="sharuto" # Tells flameshot the name of the file and is also the name of the file that is beeing uploaded
KEY="" # Your authorization key
DOMAIN="" # Your upload domain (without http:// or https://)

FILE="$IMAGEPATH$IMAGENAME.png"
flameshot gui -r > $FILE # Prompt the screenshot GUI, also append the random gibberish to /dev/null

if [ -f "$FILE" ]; then
    echo "$FILE exists."
    URL=$(curl -X POST \
      -H "Content-Type: multipart/form-data" \
      -H "User-Agent: ShareX/13.4.0" \
      -H "Bearer: $KEY" \
      -F "upload=@$FILE" "https://$DOMAIN/upload.php")
    if command -v notify-send &> /dev/null
    then
        notify-send "URL copied to clipboard" "$URL"
    fi
    printf "%s" "$URL" | xclip -sel clip
    rm "$FILE"
else 
    echo "Canceled"
fi