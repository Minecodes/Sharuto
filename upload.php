<?php
$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

$keys = array(
    1 => "key1",
);

if (isset($_SERVER['HTTP_BEARER'])) {
    $auth_header = $_SERVER['HTTP_BEARER'];
    if (in_array($auth_header, $keys)) {
        # Check if it's a POST request
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            if(!empty($_FILES) && isset(end($_FILES)['name'])) {
                $file_name = $_FILES['upload']['name'];
                $file_type = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);
                $file_ext = strtolower(end(explode('.', $_FILES['upload']['name'])));

                $allowed_extensions = array("png");

                if(in_array($file_ext, $allowed_extensions) === false){
                    echo "Extension not allowed, please choose a PNG file.";
                    exit();
                }

                $upload_path = substr(str_shuffle($characters), 0, 12);

                if(move_uploaded_file($_FILES['upload']['tmp_name'], "i/" . $upload_path.'.'.$file_type)) {
                    echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http").'://'.$_SERVER['HTTP_HOST'].'/'.$upload_path;
                } else {
                    echo "Error uploading file.";
                }
            } else {
                print_r($_FILES);
                #echo "No file was uploaded.";
            }
        } else {
            header("Location: https://http.cat/405");
            exit();
        }
    } else {
        header("Location: https://http.cat/401");
        exit();
    }
} else {
    header("Location: https://http.cat/401");
    exit();
}

?>