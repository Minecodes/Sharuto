# Sharuto

A simple upload server for PNG images.

## Installation

1. Copy the folder `i/` and the files `index.html`, `upload.php`, `viewer.php` and `oembed.php` to your webserver.
2. Change the configuration in `upload.php` and `oembed.json` to your needs:
    - **upload.php**: Change `$keys` to your own key(s)
    - **oembed.json**: Change `"author_name"` and `"author_url"` to your own username and link

## Configure your clients

### ShareX

Download the configuration file `sample.sxcu` and edit it to your own key and instance link.

### flameshot

Download the configuration file `flameshot_sample.sh` and edit it to your own key and instance link.